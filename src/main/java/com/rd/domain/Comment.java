package com.rd.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Kushtrim.Bytyqi on 6/23/2017.
 */
@Entity
public class Comment
{

    @Id
    @GeneratedValue
    private Long id;

    private String author;

    @Column(length = 512)
    private String comment;

    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "post_id")
    private Post post;

    public Comment(){}

    public Comment(String author, String comment, Date creationDate, Post post)
    {
        this.author = author;
        this.comment = comment;
        this.creationDate = creationDate;
        this.post = post;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getAuthor()
    {
        return author;
    }

    public Post getPost()
    {
        return post;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }

    public Date getCreationDate()
    {
        return creationDate;
    }

    public void setCreationDate(Date creationDate)
    {
        this.creationDate = creationDate;
    }

    @Override
    public String toString()
    {
        return "Comment{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", comment='" + comment + '\'' +
                ", creationDate=" + creationDate +
                '}';
    }

}
