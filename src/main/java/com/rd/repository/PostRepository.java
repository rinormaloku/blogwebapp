package com.rd.repository;

import com.rd.domain.Post;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Kushtrim.Bytyqi on 6/23/2017.
 */
public interface PostRepository extends CrudRepository<Post, Long> {

}