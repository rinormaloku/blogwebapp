package com.rd.repository;

import com.rd.domain.Comment;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Kushtrim.Bytyqi on 6/23/2017.
 */
public interface CommentRepository extends CrudRepository<Comment, Long> {

}