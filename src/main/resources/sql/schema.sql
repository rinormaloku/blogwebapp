CREATE TABLE post(
  post_id int NOT NULL PRIMARY KEY,
  title VARCHAR(50),
  content VARCHAR(500),
  CREATION_DATE TIMESTAMP
);
CREATE TABLE comment (
     id int NOT NULL PRIMARY KEY,
     author VARCHAR(50),
     comment  VARCHAR(50),
     CREATION_DATE TIMESTAMP,
     post_id int,
     FOREIGN KEY (post_id) REFERENCES post(post_id)
);